from flask import Flask
from flask import json
from flask import request
from flask import Response

from algorithms.evaluator import napovejPrimerA
from algorithms.evaluator import napovejPrimerB
from algorithms.evaluator import napovejPrimerC

from algorithms.modelA import trainModelA
from algorithms.modelB import trainModelB
from algorithms.modelC import trainModelC

app = Flask(__name__)

@app.route('/train', methods=["POST"])
def api_train():
    # print("Request: " + json.dumps(request.json))

    data_json = json.loads(request.data)
    trainFilename = data_json["trainFilename"]
    storyFilename = data_json["storyFilename"]
    modelId = data_json['modelId']
    outputModelFilename = data_json['outputModelFilename']

    print("Training model %s ..." % (modelId))
    result = "OK"
    if modelId == 'A':
        trainModelA(trainFilename, storyFilename, outputModelFilename)
    elif modelId == 'B':
        trainModelB(trainFilename, storyFilename, outputModelFilename)
    elif modelId == 'C':
        trainModelC(trainFilename, storyFilename, outputModelFilename)
    else:
        result = "Error"

    data = {
        "result": result
    }
    js = json.dumps(data)

    return Response(js, status=200, mimetype='application/json')

@app.route('/predict', methods = ["POST"])
def api_predict():
    #print("Request: " + json.dumps(request.json))
    
    data_json = json.loads(request.data)
    modelId = data_json['modelId']
    modelFilename = data_json['modelFilename']

    error = False
    ocena = 0
    try:
        if modelId == 'A':
            ocena = napovejPrimerA(data_json['question'].strip(), data_json['questionResponse'].strip(), modelFilename)
        elif modelId == 'B':
            ocena = napovejPrimerB(data_json['question'].strip(), data_json['questionResponse'].strip(), modelFilename)
        elif modelId == 'C':
            ocena = napovejPrimerC(data_json['question'].strip(), data_json['questionResponse'].strip(), modelFilename)
        else:
            print("Error!")
            error = True
    except:
        error = True

    ocena = float(ocena)
    if (ocena == 5.0):
        ocena = 0.5

    data = {
        "score": ocena,
        "error": False
    }

    if error:
        data = {
            "score": -1,
            "error": True
        }

    js = json.dumps(data)

    return Response(js, status=200, mimetype='application/json')


if __name__ == '__main__':
    print("Training models ...")

    #trainModelA("data/Weightless01.xlsx", "data/Weightless01.txt", "models/modelA.p")
    #trainModelB("data/Weightless01.xlsx", "data/Weightless01.txt", "models/modelB.p")
    #trainModelC("data/Weightless01.xlsx", "data/Weightless01.txt", "models/modelC.p")

    #trainModelA("data/Weightless_dataset_train.xlsx", "data/Weightless.txt", "models/modelA.p")
    #trainModelB("data/Weightless_dataset_train.xlsx", "data/Weightless.txt", "models/modelB.p")
    #trainModelC("data/Weightless_dataset_train.xlsx", "data/Weightless.txt", "models/modelC.p")

    #trainModelB("data/Weightless.xlsx", "data/Weightless.txt", "models/modelB_Weightless.p")

    trainModelA("data_test/Butalci_train.xlsx", "data/Butalci.txt", "models/modelA_Butalci.p")
    trainModelB("data_test/Butalci_train.xlsx", "data/Butalci.txt", "models/modelB_Butalci.p")
    trainModelC("data_test/Butalci_train.xlsx", "data/Butalci.txt", "models/modelC_Butalci.p")

    trainModelA("data_test/Vevericek_train.xlsx", "data/Vevericek.txt", "models/modelA_Vevericek.p")
    trainModelB("data_test/Vevericek_train.xlsx", "data/Vevericek.txt", "models/modelB_Vevericek.p")
    trainModelC("data_test/Vevericek_train.xlsx", "data/Vevericek.txt", "models/modelC_Vevericek.p")

    #trainModelA("data_test/PremrazenoSonce_train.xlsx", "data/PremrazenoSonce.txt", "models/modelA_PremrazenoSonce.p")
    #trainModelB("data_test/PremrazenoSonce_train.xlsx", "data/PremrazenoSonce.txt", "models/modelB_PremrazenoSonce.p")
    #trainModelC("data_test/PremrazenoSonce_train.xlsx", "data/PremrazenoSonce.txt", "models/modelC_PremrazenoSonce.p")
    app.run(host='0.0.0.0', port=8787)