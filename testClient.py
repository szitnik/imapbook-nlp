#!/usr/bin/python

import sys, getopt
import csv
import requests
import json
from sklearn.metrics import f1_score
import warnings
import pandas as pd
warnings.filterwarnings('ignore')

SERVER_URL = "http://localhost:8787/predict"

#TEST_FILE = "./data/Weightless_dataset_test01.csv"

TEST_FILE = "./data_test/Vevericek_test.xlsx"
MODEL_SUFFIX = "_Vevericek"

#TEST_FILE = "./data_test/Butalci_test.xlsx"
#MODEL_SUFFIX = "_Butalci"

#TEST_FILE = "./data_test/PremrazenoSonce_test.xlsx"
#MODEL_SUFFIX = "_PremrazenoSonce"


VERBOSE = False


def parseArguments(argv):
    global TEST_FILE
    global SERVER_URL
    global VERBOSE
    try:
        opts, args = getopt.getopt(argv, "hvt:s:", ["verbose", "test-file=", "server-url="])
    except getopt.GetoptError:
        print('./testClient.py -v -t <test-file> [-s <server-url>]')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('./testClient.py -v -t <test-file> [-s <server-url>]')
            sys.exit()
        elif opt in ("-t", "--test-file"):
            TEST_FILE = arg
        elif opt in ("-s", "--server-url"):
            SERVER_URL = arg
        elif opt in ("-v", "--verbose"):
            VERBOSE = True

    if TEST_FILE == "":
        print("Parameter 'test-file' is mandatory")
        sys.exit(2)


def evaluate():
    for model in ["A", "B", "C"]:
        print("\nEvaluating model %s" % model)

        trueScores = []
        predScores = []

        x1 = pd.ExcelFile(TEST_FILE)
        df = x1.parse(x1.sheet_names[0])

        for index, row in df.iterrows():
            #trueScore = int(float(row["Final.rating"].strip().replace(",", "."))*10) //CSV export style
            #print(row["Final.rating"], index)
            trueScore = int(row["Final.rating"] * 10)
            trueScores.append(trueScore)
            req = {
                "modelId": model,
                "modelFilename": "./models/model"+model+MODEL_SUFFIX+".p",
                "question": row["Question"].strip(),
                "questionResponse": row["Response"].strip()
            }

            res = requests.post(url = SERVER_URL, json = req)
            json_data = json.loads(res.text)
            predScore = abs(int(float(json_data["score"])*10))
            predScores.append(predScore)

        if VERBOSE:
            print("\tTrue scores: %s" % trueScores)
            print("\tPredicted scores: %s\n" % predScores)

        print("\t F1 (macro): %f" % f1_score(trueScores, predScores, average='macro'))
        print("\t F1 (micro): %f" % f1_score(trueScores, predScores, average='micro'))
        print("\t F1 (weighted): %f" % f1_score(trueScores, predScores, average='weighted'))


def main(argv):
    parseArguments(argv)
    evaluate()

if __name__ == "__main__":
    main(sys.argv[1:])