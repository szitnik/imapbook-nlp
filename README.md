# Simple NLP algorithms to score answers to a given question from a story

The project is based on project [https://bitbucket.org/svsadmin/imapbook](https://bitbucket.org/svsadmin/imapbook) that was a result during a Natural language processing course at the Faculty for computer and information science in Ljubljana, Fall 2018. It serves as accompanying material to the report of student's work during the lab sessions.

To set up and use the project locally, follow the steps:

0. **Create and activate conda environment**: `conda env create -f environment.yml python=3` and `conda activate imapbookNLP`
1. **Install NLTK dependencies**: `python installNLTKData.py`
2. **Run the server**: `python server.py`
3. **Build models**. *Weightless* dataset is already included. To use your own dataset, prepare the data in the same format. The file must be provided as .xlsx file, containing data in the first Excel sheet. It is important that the sheet contains the following columns: *Question*, *Response*, *Final.rating* and *Text.used.to.make.inference*.  

    It is possible to build three different models - *A*, *B* or *C*. To build a model, send an HTTP POST JSON request to *http://127.0.0.1:8787/train*:

    ```json
    { 
        "trainFilename": "/Users/.../data/Weightless_dataset_train.xlsx",
        "storyFilename": "/Users/.../data/Weightless.txt",
        "modelId": "A",
        "outputModelFilename": "/Users/.../models/modelA.p"
    }
    ```
    
    If everything is okay, HTTP status 200 should be returned:
    
    ```json
    {
        "result": "OK"
    }
    ```
4. **Predict score for an answer**. It is possible to test all the three models using *testClient.py* script (check the code for more information). To predict scores for an answer to a specific question, send an HTTP POST JSON request to *http://127.0.0.1:8787/predict*:

    ```json
    { 
        "modelId": "C", 
        "modelFilename": "/Users/.../models/modelC.p",
        "question": "How does Shiranna feel as the shuttle is taking off?", 
        "questionResponse": "Shiranna feels both excited and nervous as the shuttle is taking off." 
    }
    ```
    If everything is okay, HTTP status 200 should be returned:
    
    ```json
    {
        "error": false,
        "score": 1
    }
    ```
   
    You can also use Postman collection to directly test the server - see file *ImapBookNLP.postman_collection.json*.
    
 
To use docker installation, run `docker-run.sh` to build and run docker image locally. You can also directly download and run docker image from dockerhub using command `docker run -d -p 8787:8787 szitnik/imapbook-nlp:latest`. The root of the project resides in folder `/imapbookNlp`.