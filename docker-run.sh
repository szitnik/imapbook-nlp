# Building and running locally
docker build -t imapbook-nlp .
docker run -d -p 8787:8787 imapbook-nlp:latest

# Running directly from dockerhub
#docker run -d -p 8787:8787 szitnik/imapbook-nlp:latest

# Publishing to dockerhub
#docker login                                   # Log in this CLI session using your Docker credentials
#docker tag <image> username/repository:tag     # Tag <image> for upload to registry
#docker push username/repository:tag            # Upload tagged image to registry
#docker run username/repository:tag             # Run image from a registry