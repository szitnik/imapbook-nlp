FROM ubuntu:18.04

WORKDIR /imapbookNlp
ADD ./ .


RUN apt-get update \
    && apt-get install -y wget \
    && wget https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh \
    && bash Anaconda3-2019.03-Linux-x86_64.sh -b -p /anaconda3  \
    && /anaconda3/bin/conda install -y numpy scikit-learn \
    && /anaconda3/bin/conda install -y -c anaconda gensim nltk flask paramiko \
    && /anaconda3/bin/python installNLTKData.py

EXPOSE 8787

ENTRYPOINT /anaconda3/bin/python server.py