import pickle
from algorithms import napovedovanje, helpers
import gensim
import numpy


def trainModelsHelper(ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, vektorji05, modeli):
    n_modeli1 = {}
    n_modeli2 = {}
    maks_vrednosti = {}

    for vpr in vektorji1.keys():
        X, y = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "cossim")
        X2, y2 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "kontekst")
        maks_vrednosti["kontekst"] = max(X2)
        X2 = X2 / max(X2)
        X = numpy.concatenate((X, X2), axis=1)
        X3, y3 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "w2vNajsim")
        maks_vrednosti["w2vNajsim"] = max(X3)
        X3 = X3 / max(X3)
        X = numpy.concatenate((X, X3), axis=1)

        if vpr in vektorji05.keys():
            X1, y1 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji05, modeli, "cossim")
            X1 /= max(X1)
            maks_vrednosti["cossim05"] = max(X1)
            X = numpy.concatenate((X, X1), axis=1)

        n_modeli1[vpr] = napovedovanje.ridgeKlasifikator(X, y)
        n_modeli2[vpr] = napovedovanje.randomForest(X, y)

    return n_modeli1, n_modeli2, maks_vrednosti

def trainModelB(trainFilename, storyFilename, outputModelFilename):
    vsi, delezi = helpers.nalozi(trainFilename)
    u_ = helpers.naloziSinglesPravilnih(trainFilename)
    zgodba = helpers.naloziZgodbo(storyFilename)

    u1A, u05A, u0A = helpers.pretvoriVslovar(u_)

    besedisce = napovedovanje.vrniBesedisceA(u1A) + zgodba
    besedisce = [napovedovanje.predelaj(b) for b in besedisce]
    idf, tfidf_model = helpers.pridobiIdf([" ".join(b) for b in besedisce])

    W2Vmodel = gensim.models.Word2Vec(
            besedisce,
            size=150,
            window=10,
            min_count=2,
            workers=10)
    W2Vmodel.train(besedisce, total_examples=len(besedisce), epochs=10)

    modeli = napovedovanje.Modeli()
    modeli.tf_idf = tfidf_model
    modeli.W2Vmodel = W2Vmodel
    modeli.Idf_ = idf

    #Vsi podatki
    u1, u05, u0 = helpers.pretvoriVslovar(vsi)

    vu1 = napovedovanje.vrniVektorje(u1)

    vu05 = napovedovanje.vrniVektorje(u05)

    n_modeli1, n_modeli2, maks_vrednosti = trainModelsHelper(u1, u05, u0, vu1, vu05, modeli)

    pickle.dump((n_modeli1, n_modeli2, maks_vrednosti, u1, vu1, vu05, delezi, modeli), open(outputModelFilename, "wb" ))


#trainModelB("../data/Weightless_dataset_train.csv", "../data/Weightless.txt", "../models/modelB.p")