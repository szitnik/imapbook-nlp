import pickle
from algorithms import napovedovanje, helpers
import gensim

def trainModelA(trainFilename, storyFilename, outputModelFilename):

    ucni = helpers.naloziSinglesPravilnih(trainFilename)

    u1, u05, u0 = helpers.pretvoriVslovar(ucni)

    zgodba = helpers.naloziZgodbo(storyFilename)

    besedisce = napovedovanje.vrniBesedisceA(u1) + zgodba
    besedisce = [napovedovanje.predelaj(b) for b in besedisce]
    idf, tfidf_model = helpers.pridobiIdf([" ".join(b) for b in besedisce])

    W2Vmodel = gensim.models.Word2Vec(
            besedisce,
            size=150,
            window=10,
            min_count=2,
            workers=10)
    W2Vmodel.train(besedisce, total_examples=len(besedisce), epochs=10)

    modeli = napovedovanje.Modeli()
    modeli.tf_idf = tfidf_model
    modeli.W2Vmodel = W2Vmodel
    modeli.Idf_ = idf

    u_vektorji = napovedovanje.vrniVektorjeWordNetA(u1, idf)

    def napovejPrimerA(vprasanje, odgovor):
        v = napovedovanje.Vprasanje(u1[vprasanje].tip, vprasanje, u1[vprasanje].kontekst)
        v.odgovori.append(odgovor)
        t_vpr = {}

        t_vpr[vprasanje] = v
        w2vNajSim = napovedovanje.vrniW2VNajSim(vprasanje, t_vpr, u_vektorji, modeli)
        return w2vNajSim

    pickle.dump((u1, u_vektorji, modeli), open( outputModelFilename, "wb" ))

#trainModelA("../data/Weightless_dataset_train.csv", "../data/Weightless.txt", "../models/modelA.p")